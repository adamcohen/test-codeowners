#!/usr/bin/env sh

set -e

function print_usage() {
    echo ""
    echo "USAGE:"
    echo "  ./compare_reports.sh [OPTIONS]"
    echo ""
    echo "EXAMPLE:"
    echo "  ./compare_reports.sh dast actual.json expected.json"
    echo ""
    echo "OPTIONS:"
    echo "  -h                                                           Display this help message."
    echo "  <report-type> <path-to-actual-file> <path-to-expected-file>  Compare reports. \`report-type\` must be one of:"
    echo "                                                                 - sast: Static Application Security Testing"
    echo "                                                                 - dast: Dynamic Application Security Testing"
    echo "                                                                 - cs:   Container Scanning"
    echo "                                                                 - ds:   Dependency Scanning"
    echo "                                                                 - lm:   License Management"
    echo "                                                                 - ls:   License Scanning"
    echo "                                                                 - cq:   Code Quality"
    echo "                                                                 - it:   Integration Test"
}

function filter_for_report_type() {
    set -e

    local local_report_type jq_filter

    local_report_type=$1

    case "$local_report_type" in
        "cs")
            jq_filter="del(.version) |
                       del(.vulnerabilities[]|.location.image) |
                       del(.vulnerabilities[].id) |
                       del(.remediations[].fixes[].id) |
                       .vulnerabilities |= map_values(.links |= (. // [])) |
                       .vulnerabilities |= map_values(.identifiers |= (. // [])) |
                       (.. | arrays) |= sort"
        ;;
        "lm" | "ls" | "cq")
            jq_filter="(.. | arrays) |= sort"
            ;;
        "dast")
            jq_filter='del(.["@generated", "@version"]) | (.. | arrays) |= sort'
            ;;
        "ds" | "sast" | "it")
            jq_filter="del(.version) |
                       del(.vulnerabilities[].id) |
                       del(.remediations[].fixes[].id) |
                       .vulnerabilities |= map_values(.links |= (. // [])) |
                       .vulnerabilities |= map_values(.identifiers |= (. // [])) |
                       (.. | arrays) |= sort"
            ;;
        *)
            >&2 echo "Error: Unknown report type '$local_report_type'. Please provide the" \
                "report type as the first argument.  Report type must be one of:" \
                "<sast|dast|cs|ds|lm|ls|cq|it>"
            ;;
    esac

    echo $jq_filter
}

function sanitize_report() {
    set +e

    local error sanitized_output_file jq_filter report_name local_report_type

    local_report_type=$1
    report_name=$2

    jq_filter=$(filter_for_report_type $local_report_type)

    if [ -z "$jq_filter" ]; then
        exit 1
    fi

    sanitized_output_file=$(/usr/bin/env echo -n $report_name | sed -e "s/\(.*\)\.json/\1-sanitized\.json/")

    error=$(/usr/bin/env jq -e "$jq_filter" $report_name 2>&1 > $sanitized_output_file)

    if [ $? -ne 0 ]; then
        rm $sanitized_output_file

        if [ "$error" != "${error%"cannot be sorted, as it is not an array"*}" ]; then
            >&2 echo "Error: jq encountered an error while attempting to parse the file:" \
                "'$report_name': '$error'. It appears as though the JSON file '$report_name'" \
                "consists of some objects which contain array elements, while other objects" \
                "are missing these elements. In order for jq to be able to sort the file, all" \
                "objects (entries) in the file must have the _same_ structure. Please ensure" \
                "that you've set the correct report-type value for the given report. If the report-type" \
                "has been set correctly, then you'll need to update the \`jq_filter\` variable" \
                "of this script to insert blank \`[]\` entries for these missing array elements" \
                "using the \`map_values\` function."
            exit 1
        fi

        >&2 echo "Error: jq encountered an error while attempting to parse the file '$report_name'. '$error'"
        exit 1
    fi

    echo $sanitized_output_file
}

function parse_command_line_options() {
    set -e

    # Transform long options to short ones
    for arg in "$@"; do
        shift
        case "$arg" in
            "--help")   set -- "$@" "-h" ;;
            *)          set -- "$@" "$arg"
        esac
    done

    # Parse short options
    OPTIND=1
    while getopts "h" opt
    do
        case "${opt}" in
            "h") print_usage; exit 0 ;;
            "?") print_usage >&2; exit 1 ;;
        esac
    done
    shift $(expr $OPTIND - 1) # remove options from positional parameters

    report_type=$1
    actual_report=$2
    expected_report=$3

    if [ -z $report_type ]; then
        echo "Error: Please provide the type of the report."
        print_usage
        exit 1
    fi

    if [ -z $actual_report ]; then
        echo "Error: Please provide the path to the actual file."
        print_usage
        exit 1
    fi

    if [ ! -f "$actual_report" ]; then
        echo "Error: report with path '$actual_report' does not exist."
        print_usage
        exit 1
    fi

    if [ -z $expected_report ]; then
        echo "Error: Please provide the path to the expected file."
        print_usage
        exit 1
    fi

    if [ ! -f "$expected_report" ]; then
        echo "Error: report with path '$expected_report' does not exist."
        print_usage
        exit 1
    fi
}

function sanitize_and_diff_reports() {
    set -e

    local sanitized_actual_report sanitized_expected_report
    local local_actual_report local_expected_report local_report_type

    local_report_type=$1
    local_actual_report=$2
    local_expected_report=$3

    sanitized_actual_report=$(sanitize_report $local_report_type $local_actual_report)
    sanitized_expected_report=$(sanitize_report $local_report_type $local_expected_report)

    set +e

    diff -b -u $sanitized_expected_report $sanitized_actual_report
    diff_failed=$?

    rm -f $sanitized_actual_report
    rm -f $sanitized_expected_report

    if [ $diff_failed -ne 0 ]; then
        exit 1
    fi
}

# Global variables
report_type=""
actual_report=""
expected_report=""

parse_command_line_options $@
sanitize_and_diff_reports $report_type $actual_report $expected_report
