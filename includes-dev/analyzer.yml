# When using dind, it's wise to use the overlayfs driver for
# improved performance.
variables:
  DOCKER_DRIVER: overlay2
  MAJOR: 1
  TMP_IMAGE: $CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA

services:
  - docker:19.03.5-dind

stages:
  - go
  - build
  - test
  - performance-metrics
  - tag
  - deploy
  - post

.go:
  image: golang:1.13.7
  stage: go

go build:
  extends: .go
  variables:
    CGO_ENABLED: 0
  script:
    - go get ./...
    - go build -o ${CI_PROJECT_DIR}/analyzer
  artifacts:
    paths:
    - analyzer

go test:
  extends: .go
  script:
    - go get ./...
    - go test -race -cover -v ./...
  coverage: '/coverage: \d+.\d+% of statements/'

go lint:
  extends: .go
  script:
    - go get -u golang.org/x/lint/golint
    - golint -set_exit_status ./...

build commit:
  image: docker:stable
  stage: build
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $TMP_IMAGE .
    - docker push $TMP_IMAGE

check image size:
  stage: test
  variables:
    PATH_NAME: "tmp"
  image: alpine:3.9
  script:
    - apk add --no-cache jq
    - "repository_id=$(wget -q -O- --header \"PRIVATE-TOKEN: $API_TOKEN\" \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories\" | jq --arg NAME ${PATH_NAME} '.[] | select(.name == $NAME) | .id')"
    - "current_size=$(wget -q -O- --header \"PRIVATE-TOKEN: $API_TOKEN\" \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${repository_id}/tags/${CI_COMMIT_SHA}\" | jq '.total_size')"
    - echo $current_size
    - allowed_image_size=${MAX_IMAGE_SIZE_BYTE:-419430400}
    - echo $allowed_image_size
    - test $allowed_image_size -gt $current_size

check analyzer scan duration:
  stage: performance-metrics
  image: alpine:3.9
  script:
    - scan_duration=$(cat analyzer_scan_duration_in_seconds.txt)
    - echo $scan_duration
    - allowed_scan_duration=${MAX_SCAN_DURATION_SECONDS:-60}
    - echo $allowed_scan_duration
    - test $allowed_scan_duration -gt $scan_duration
  dependencies:
    - integration test
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - exists:
        - test/expect/*
        - test/fixtures/*

integration test:
  image: docker:stable
  stage: test
  script:
    - REPORT_FILENAME=${REPORT_FILENAME:-gl-sast-report.json}
    - apk update && apk add jq
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - START_TIME=`date +%s`
    - docker run --volume ${CI_PROJECT_DIR}/test/fixtures:/tmp/project --env CI_PROJECT_DIR=/tmp/project --env DS_EXCLUDED_PATHS --env SAST_EXCLUDED_PATHS $TMP_IMAGE /analyzer run
    - echo $((`date +%s` - $START_TIME)) > analyzer_scan_duration_in_seconds.txt
    - echo "If this integration fails and you believe the expected report is wrong, then patch it using the following diff."
    - echo "$ cat report.diff | patch -Np1 test/expect/$REPORT_FILENAME && git commit -m 'Update expectation' test/expect/$REPORT_FILENAME"
    - wget https://gitlab.com/gitlab-org/security-products/ci-templates/-/raw/master/scripts/compare_reports.sh
    - sh ./compare_reports.sh it test/fixtures/$REPORT_FILENAME test/expect/$REPORT_FILENAME

  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - exists:
        - test/expect/*
        - test/fixtures/*
  artifacts:
    paths:
    - analyzer_scan_duration_in_seconds.txt

.docker_tag:
  image: docker:stable
  stage: tag
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export SOURCE_IMAGE=$TMP_IMAGE
    - export TARGET_IMAGE=$CI_REGISTRY_IMAGE:${IMAGE_TAG:-$CI_JOB_NAME}
    - docker pull $SOURCE_IMAGE
    - docker tag $SOURCE_IMAGE $TARGET_IMAGE
    - docker push $TARGET_IMAGE
  dependencies:
    - go build

.qa-downstream-ds:
  stage: test
  variables:
    SAST_DISABLED: "true"
    CONTAINER_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    DS_DEFAULT_ANALYZERS: ""
    DS_ANALYZER_IMAGES: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
  trigger:
    project: ""
    branch: no_dind-FREEZE
    strategy: depend

.qa-downstream-sast:
  stage: test
  variables:
    DEPENDENCY_SCANNING_DISABLED: "true"
    CONTAINER_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    SAST_DEFAULT_ANALYZERS: ""
    SAST_ANALYZER_IMAGES: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SAST_DISABLE_DIND: "false"
  trigger:
    project: ""
    branch: master
    strategy: depend

.qa-downstream-cs:
  stage: test
  variables:
    SAST_DISABLED: "true"
    DEPENDENCY_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
  trigger:
    project: ""
    branch: master
    strategy: depend

tag branch:
  extends: .docker_tag
  variables:
    # CAUTION: by preferring `SLUG` over `NAME` we can properly handle non-alphanumeric
    # characters, but this may limit our tags to 63chars or raise potential conflicts.
    IMAGE_TAG: $CI_COMMIT_REF_SLUG
  only:
    - branches
  except:
    - master

tag edge:
  extends: .docker_tag
  variables:
    IMAGE_TAG: edge
  only:
    - master

tag version:
  extends: .docker_tag
  before_script:
    - export IMAGE_TAG=${CI_COMMIT_TAG/v/}
    - echo "Checking that $CI_COMMIT_TAG is last in the changelog"
    - test "$(grep '^## v' CHANGELOG.md |head -n 1)" = "## $CI_COMMIT_TAG"
  only:
    - tags
  when: manual
  allow_failure: false

.release:
  extends: .docker_tag
  stage: deploy
  only:
    - tags

major:
  extends: .release
  variables:
    IMAGE_TAG: $MAJOR

latest:
  extends: .release

include:
  - template: SAST.gitlab-ci.yml
  - template: Container-Scanning.gitlab-ci.yml
  - template: License-Management.gitlab-ci.yml

container_scanning:
  variables:
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/tmp

benchmark:
  stage: post
  only:
    - tags
  trigger: gitlab-org/security-products/sast-benchmark
